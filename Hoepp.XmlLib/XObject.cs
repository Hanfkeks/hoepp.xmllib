﻿using System;
using System.Xml.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Hoepp.XmlLib
{
    [AttributeUsage(AttributeTargets.Field)]
    public class XObject : Attribute
    {
        public string Name
        {
            get
            {
                if (_name == null) {
                    //_name = "";
                }
                return _name;
            }
            set {
                _name = value;
            }
        }

        private string _name = null;

        private bool _polymorph = false;
        public bool Polymorph
        {
            get
            {
                return _polymorph;
            }
            set
            {
                _polymorph = value;
            }
        }

        public XObject(string name)
        {
            Name = name;
        }

        public XObject(string name, bool polymorph)
        {
            Name = name;
            Polymorph = polymorph;
        }


    }
}
