﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Hoepp.XmlLib
{
    public class XKnowledge
    {
        public static Dictionary<Type, Func<XElement, object>> CustomDeserializations = new Dictionary<Type, Func<XElement, object>>();
        public static Dictionary<Type, Func<object, XElement>> CustomSerializations = new Dictionary<Type, Func<object, XElement>>();


    }
}
