﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Xml.Linq;
using System.Threading;
using System.Globalization;
using System.Collections;
using System.Runtime.CompilerServices;

namespace Hoepp.XmlLib
{
    [AttributeUsage(AttributeTargets.Class)]
    public class XClass : Attribute
    {

        public string Name;
        public Type ClassType;

        public XClass(string name, Type classType)
        {
            ClassType = classType;
            Name = name;
            XDeserializer.AddBlueprint(name, classType);
        }

        public static void InitCulture()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        }

        public static XElement Serialize(object obj, string customName = null)
        {
            string name = customName;

            if (obj == null)
            {
                XElement preElem = new XElement(name);
                return preElem;
            }

            Type objtype = obj.GetType();
            XClass xc = objtype.GetCustomAttribute<XClass>();

            if (name == null)
            {

                if (xc == null)
                {
                    name = objtype.ToString();
                }
                else
                {
                    name = xc.Name;
                }
            }

            XElement elem = new XElement(name);
            if (xc == null)
            {
                if (obj is double || obj is float || obj is int || obj is long || obj is char || obj is bool)
                {
                    elem.Value = obj.ToString();
                    return elem;
                }
                else if(obj is string)
                {
                    elem.Value = (string)obj;
                    return elem;
                }
                else if (obj is Enum)
                {
                    elem.Value = ((Enum)obj).ToString("g");
                    return elem;
                }

                if (objtype.IsArray)
                {
                    object[] objarray = (object[])obj;
                    foreach (object lobj in objarray)
                    {
                        elem.Add(XClass.Serialize(lobj, "Element"));
                    }
                }
                if (objtype.IsGenericType)
                {
                    Type[] args = objtype.GetGenericArguments();
                    Type genericType = objtype.GetGenericTypeDefinition();
                    switch (args.Length)
                    {
                        case 1:
                            if (genericType == (typeof(List<>)))
                            {
                                IEnumerable ungenericList = (IEnumerable)obj;
                                foreach (object lobj in ungenericList)
                                {
                                    elem.Add(XClass.Serialize(lobj, "Element"));
                                }
                                return elem;
                            }


                            break;
                        case 2:
                            if (genericType == (typeof(Dictionary<,>)))
                            {
                                IEnumerable ungenericDict = (IEnumerable)obj;
                                foreach (object kvpobj in ungenericDict)
                                {
                                    Type kvptype = kvpobj.GetType();

                                    XElement kvpelem = new XElement("KeyValuePair");

                                    XElement keyelem = Serialize(kvptype.GetProperty("Key").GetValue(kvpobj), "Key");
                                    kvpelem.Add(keyelem);

                                    XElement valelem = Serialize(kvptype.GetProperty("Value").GetValue(kvpobj), "Value");
                                    kvpelem.Add(valelem);

                                    elem.Add(kvpelem);
                                    
                                }
                                return elem;
                            }
                            break;
                    }


                }
            }
            else
            {
                //It appears as if our object is a complex type
                FieldInfo[] allFields = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

                foreach (FieldInfo info in allFields)
                {
                    XObject xo = info.GetCustomAttribute<XObject>();
                    if (xo != null)
                    {
                        Type fieldType = info.ReflectedType;
                        object subobject = info.GetValue(obj);
                        XClass subXClass = fieldType.GetCustomAttribute<XClass>();

                        string xoname = xo.Name;
                        if (xoname == null)
                        {
                            xoname = info.Name;
                        }

                        if (subXClass != null)
                        {

                            elem.Add(XClass.Serialize(subobject, xoname));
                        }
                        else
                        {
                            Console.WriteLine("Type of " + xoname + " is " + fieldType.ToString());
                        }

                    }
                }
                return elem;
            }
            return elem; //Could be unknown and therefore empty
        }

    }
}
