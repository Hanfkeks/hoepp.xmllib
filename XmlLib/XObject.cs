﻿using System;
using System.Xml.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Hoepp.XmlLib
{
    [AttributeUsage(AttributeTargets.Field)]
    public class XObject : Attribute
    {
        public string Name
        {
            get
            {
                if (_name == null) {
                    //_name = "";
                }
                return _name;
            }
            set {
                _name = value;
            }
        }

        private string _name = null;

        public XObject(string name)
        {
            Name = name;
        }

        public XElement Serialize()
        {
            XElement elem = new XElement(Name);

            elem.Value = this.Name;

            

            return elem;
        }


    }
}
