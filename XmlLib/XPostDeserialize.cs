﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hoepp.XmlLib
{
    /// <summary>
    /// Pretty much a init Method. Must not have any 
    /// </summary>
    
    [AttributeUsage(AttributeTargets.Method)]
    public class XPostDeserialize:Attribute
    {
        public XPostDeserialize()
        {

        }
    }
}
