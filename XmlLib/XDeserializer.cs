﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace Hoepp.XmlLib
{
    public static class XDeserializer
    {
        public static Dictionary<string, Type> Blueprints = new Dictionary<string, Type>();

        public static void AddBlueprint(string name, Type classtype)
        {
            if (Blueprints.ContainsKey(name)) { Blueprints[name] = classtype; }
            else { Blueprints.Add(name, classtype); }
        }

        public static object DeserializeBasicType(XElement elem, Type type)
        {
            string typename = type.Name;
            try
            {
                if (typename == (typeof(int)).Name)
                {

                    return int.Parse(elem.Value);

                }
                else if (typename == (typeof(string)).Name)
                {

                    return elem.Value;

                }
                else if (typename == (typeof(float)).Name)
                {

                    return float.Parse(elem.Value);

                }
                else if (typename == (typeof(long)).Name)
                {

                    return long.Parse(elem.Value);

                }
                else if (typename == (typeof(double)).Name)
                {

                    return double.Parse(elem.Value);

                }
                else if (typename == (typeof(bool)).Name)
                {

                    return bool.Parse(elem.Value);

                }

            }
            catch (FormatException)
            {
                Console.WriteLine("Deserialization Incomplete: Format invalid");
                
            }
            return null;
        }

        public static object Deserialize(XElement elem, Type type)
        {
            object createdObject = DeserializeBasicType(elem,type);
            if (createdObject != null)
            {
                return createdObject;
            }
            else if (type.IsArray)
            {
                Type elemtype = type.GetElementType();
                List<object> subelemList = new List<object>();
                foreach (XElement subelem in elem.Elements())
                {
                    subelemList.Add(Deserialize(subelem, elemtype));
                }

                object arrayobj = Activator.CreateInstance(type,new object[]{subelemList.Count});
                int i = 0;
                foreach (object subobj in subelemList)
                {
                    
                    type.GetMethod("SetValue",new[] { elemtype , typeof(int) }).Invoke(arrayobj, new[] { subobj, i });
                    i++;
                }


                return arrayobj;
            }

            Type[] args = type.GetGenericArguments();
            if (args.Length>0)
            {
                Type genericType = type.GetGenericTypeDefinition();
                if (args.Length == 1)
                {
                    if (genericType == (typeof(List<>)))
                    {
                        Type elemtype = args[0];
                        List<object> subelemList = new List<object>();
                        

                        
                        object listobj = Activator.CreateInstance(type);

                        foreach (XElement subelem in elem.Elements())
                        {
                            object listelem = Deserialize(subelem, elemtype);
                            type.GetMethod("Add").Invoke(listobj, new[] { listelem });
                        }

                        
                        return listobj;
                    }

                }
                else if (args.Length == 2)
                {
                    if (genericType == (typeof(Dictionary<,>)))
                    { 
                        Type keytype = args[0];
                        Type valuetype = args[1];

                        object dictobj = Activator.CreateInstance(type);

                        foreach (XElement subelem in elem.Elements())
                        {
                            object key = null;
                            object val = null;
                            foreach (XElement minorelem in subelem.Elements())
                            {
                                if (minorelem.Name == "Key") { key = Deserialize(minorelem,keytype); }
                                if (minorelem.Name == "Value") { val = Deserialize(minorelem, valuetype); }
                            }
                            

                            type.GetMethod("Add").Invoke(dictobj,new object[]{ key, val});
                            
                        }

                        
                        


                        return dictobj;

                    }
                }
            }

            ConstructorInfo ci = type.GetConstructor(new Type[0]);
            if (ci == null)
            {
                Console.WriteLine("Deserialization Failed, no Constructor without parameters found");
                return null;
            }
            createdObject = ci.Invoke(new object[0]);

            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo info in fields)
            {
                XObject xo = info.GetCustomAttribute<XObject>();
                if (xo != null)
                {
                    string fieldname = xo.Name;
                    Type fieldtype = info.FieldType;
                    foreach (XElement subelem in elem.Elements())
                    {
                        if (subelem.Name == fieldname)
                        {
                            info.SetValue(createdObject,Deserialize(subelem,fieldtype));
                        }
                    }
                }
            }

            return createdObject;
        }

        /// <summary>
        /// Deserialize a complex XClass
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="typename"></param>
        public static object Deserialize(XElement elem, string typename)
        {
            Type t = Blueprints[typename];
            if (t == null)
            {
                Console.WriteLine("Deserialization Failed, Blueprint not registered!");
                return null;
            }
            return Deserialize(elem, t);
        }

    }
}
