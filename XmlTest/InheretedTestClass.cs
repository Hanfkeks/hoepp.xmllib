﻿using Hoepp.XmlLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlTest
{
    [XClass(nameof(InheretedTestClass), typeof(InheretedTestClass))]
    public class InheretedTestClass:TestClass
    {
        [XObject("SomeOtherInteger")]
        public int AdditionalInteger = 123123123;

        public InheretedTestClass() { }
    }
}
