﻿using Hoepp.XmlLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XmlTest
{
    class Program
    {
        static void Main(string[] args)
        {
            TestClass tc1 = new TestClass();
            TestClass tc2 = new TestClass();
            tc1.TestObject = tc2;
            tc1.SomeInteger = 1337;
            tc1.SomeString = "Fourtytwo";
            tc1.SomeList.Add(1);
            tc1.SomeList.Add(2);
            tc1.SomeList.Add(3);
            tc2.SomeList.Add(4711);
            tc2.SomeArray = new string[] { "Bro", "what", "is", "up" };
            tc1.SomeDict.Add("one",1);
            tc1.SomeDict.Add("two", 2);
            tc1.SomeDict.Add("three", 3);
            tc1.setnun( 44);

            InheretedTestClass itc = new InheretedTestClass();
            itc.SomeList.Add(1);
            itc.SomeList.Add(2);
            itc.SomeList.Add(3);
            itc.time = DateTime.Now;

            XClass.InitCulture();
            XKnowledge.CustomSerializations.Add(typeof(DateTime),SerializeDatetime);
            XKnowledge.CustomDeserializations.Add(typeof(DateTime), DeserializeDatetime);
            XElement serializedTc1 = XClass.Serialize(tc1);
            XElement serializedItc = XClass.Serialize(itc);
            Console.WriteLine(serializedTc1.ToString());
            Console.WriteLine("");
            Console.WriteLine("Inhereted Class:");
            Console.WriteLine("");
            Console.WriteLine(serializedItc);

            Console.WriteLine("");
            Console.WriteLine("Re- and Deserialization");
            Console.WriteLine("");

            InheretedTestClass itc_D = (InheretedTestClass)XDeserializer.Deserialize(serializedItc, (nameof(InheretedTestClass)).ToString() );
            XElement itc_RD = XClass.Serialize(itc_D);
            Console.WriteLine(itc_RD.ToString());

            Console.WriteLine("");
            Console.WriteLine("Original Testclass after Re- and Deserialization");
            Console.WriteLine("");

            TestClass tc1_D = (TestClass)XDeserializer.Deserialize(serializedTc1, (nameof(TestClass)).ToString());
            XElement tc1_DR = XClass.Serialize(tc1_D);
            Console.WriteLine(tc1_DR.ToString());

            Console.WriteLine();

            Console.ReadKey();
            Console.WriteLine("Phase 2: ");
            Console.WriteLine("");
            PolymorphTestclass polytest = new PolymorphTestclass();
            polytest.PolyList.Add(itc);
            XElement ser = XClass.Serialize(polytest);
            Console.WriteLine(ser.ToString());
            Console.WriteLine("");
            Console.WriteLine("Deserializing: ");
            Console.WriteLine("");
            Console.WriteLine(XClass.Serialize(XDeserializer.Deserialize(ser,"PolymorphTestclass")).ToString());
            Console.ReadKey();
        }

        public static XElement SerializeDatetime(object dt)
        {
            DateTime time = (DateTime)dt;
            XElement elem = new XElement("DateTime");
            elem.Value = time.ToBinary().ToString();
            return elem;
        }

        public static object DeserializeDatetime(XElement elem)
        {
            return DateTime.FromBinary(long.Parse(elem.Value));
        }

    }
}
