﻿using Hoepp.XmlLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlTest
{
    [XClass(nameof(TestClass),typeof(TestClass))]
    public class TestClass
    {
        [XObject(nameof(SomeInteger))]
        public int SomeInteger = 12;

        [XObject(nameof(SomeList))]
        public List<int> SomeList = new List<int>();

        [XObject(nameof(SomeArray))]
        public string[] SomeArray;

        [XObject(nameof(PrivFloat))]
        private float PrivFloat = 0.01f;

        [XObject("SomeRenamedDict")]
        public Dictionary<string, int> SomeDict = new Dictionary<string, int>();

        [XObject(nameof(Color))]
        protected ConsoleColor Color = ConsoleColor.Red;

        public int InvisibleInteger = 123123;

        [XObject(nameof(SomeString))]
        public string SomeString = "";

        [XObject(nameof(TestObject))]
        public TestClass TestObject = null;

        [XObject("NotUniqueName")]
        private float NotUniqueName;

        public void setnun(float nun)
        {
            NotUniqueName = nun;
        }


        [XObject(nameof(DateTime))]
        public DateTime time;

        public TestClass() { }

    }
}
