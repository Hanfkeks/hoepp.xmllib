﻿using Hoepp.XmlLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlTest
{
    [XClass(nameof(PolymorphTestclass), typeof(PolymorphTestclass))]
    public class PolymorphTestclass
    {

        [XObject("PolyList")]
        public List<TestClass> PolyList = new List<TestClass>();

    }
}
